angular.module('starter.controllers', [])


.service('BasketService', function() {
  
basket = 5;

this.returnBasket = function(){
	return basket;
}
  
})


.controller('AppCtrl', function($scope, $ionicModal, $timeout,$localStorage,$state) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  $scope.$on('$ionicView.enter', function(e) {
	
  $scope.checkParams = function()
  {  
	  $scope.userId = $localStorage.userid;
	  $scope.userTypeArray = ['לקוח רגיל','שליח','ספק','מנהל','לקוח עיסקי'];
	  $scope.userStatusId =  $localStorage.type;
	  $scope.userStatus = $scope.userTypeArray[$scope.userStatusId];
	  $scope.loggedinFullName = $localStorage.name;	  
  }  
  
  $scope.checkParams();


 
  $scope.LogOut = function()
  {
	$localStorage.userid = '';
	$localStorage.name = '';
	$localStorage.email = '';
	$localStorage.phone = '';
	$localStorage.address = '';
	$localStorage.type = '';
	$scope.checkParams();
	$state.go('app.main');	  
  }
  
 });

 
})



.controller('LoginRegisterCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope) {

	if ($localStorage.userid)
	{
		//$state.go('app.main');
	}
	
	$scope.BasketPrice = 0;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.Redirect = $stateParams.Redirect;

	
	$scope.login = 
	{
		"email" : "",
		"password" : ""
	}
	
	$scope.forgot = 
	{
		"email" : ""
	}
	
	$scope.register = 
	{
		"name" : "",
		"address" : "",
		"phone" : "",
		"email" : "",
		"password" : ""
	}
	
	
	$scope.LoginBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailregex = /\S+@\S+\.\S+/;
		
		if ($scope.login.email =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מייל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else if (!emailregex.test($scope.login.email))
		{
			$ionicPopup.alert({
			 title: 'מייל לא תקין יש לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}		
		else if ($scope.login.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא סיסמה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
		}

		else
		{
			login_data = 
			{
				"email" : $scope.login.email,
				"password" : $scope.login.password
			}					
			$http.post($rootScope.Host+'/login.php',login_data).success(function(data)
			{
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: 'שם משתמש או סיסמה שגוים יש לתקן',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });					
				}
				else
				{
					$localStorage.userid = data.response.userid;
					$localStorage.name = data.response.name;
					$localStorage.email = data.response.email;
					$localStorage.phone = data.response.phone;
					$localStorage.address = data.response.address;
					$localStorage.type = data.response.type;
					//$state.go('app.main');
					if ($scope.Redirect == 1)
					{
						window.location.href = "#/app/orderform";
					}
					else if ($scope.Redirect == 2)
					{
						window.location.href = "#/app/specialrequests";
					}
					else if ($scope.Redirect == 3)
					{
						window.location.href = "#/app/main";
					}    				  
				  
					else
					{
						window.location.href = "#/app/shops";
					}
					
				}
			});			
		}
	}
	
	$scope.sendPassBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailregex = /\S+@\S+\.\S+/;
		
		if ($scope.forgot.email =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מייל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else if (!emailregex.test($scope.forgot.email))
		{
			$ionicPopup.alert({
			 title: 'מייל לא תקין יש לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else
		{
			forgot_data = 
			{
				"email" : $scope.forgot.email,
				"send" :  1
			}					
			$http.post($rootScope.Host+'/forgot_pass.php',forgot_data).success(function(data)
			{
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: 'מייל לא נמצא במערכת נא לנסות שוב',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });		
				}
				else
				{
					$ionicPopup.alert({
					 title: 'סיסמה נשלחה בהצלחה למייל שהזנת',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });		

					$state.go('app.login');				   
				}

				$scope.forgot.email = '';
				
				
			});	
		}
	}
	
	$scope.RegisterBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailregex = /\S+@\S+\.\S+/;

		if ($scope.register.name =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא שם מלא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}

		else if ($scope.register.address =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מגורים',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}

		else if ($scope.register.phone =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא מספר טלפון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}

		
		else if ($scope.register.email =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מייל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else if (!emailregex.test($scope.register.email))
		{
			$ionicPopup.alert({
			 title: 'מייל לא תקין יש לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.register.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא סיסמה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		
		else
		{
			register_data = 
			{
				"name" : $scope.register.name,
				"address" : $scope.register.address,
				"phone" : $scope.register.phone,
				"email" : $scope.register.email,
				"password" : $scope.register.password
			}					
			$http.post($rootScope.Host+'/register.php',register_data).success(function(data)
			{
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: 'אימייל כבר בשימוש יש לבחר מייל אחר',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });	

					$scope.register.email = '';
				}
				else
				{
					$localStorage.userid = data.response.userid;
					$localStorage.name = $scope.register.name;
					$localStorage.email = $scope.register.email;
					$localStorage.phone = $scope.register.phone;
					$localStorage.address = $scope.register.address;
					$localStorage.type = data.response.type;
					//$state.go('app.main');
    				//window.location.href = "#/app/orderform";
			
					if ($scope.Redirect == 1)
					{
						window.location.href = "#/app/orderform";
					}
					else if ($scope.Redirect == 2)
					{
						window.location.href = "#/app/specialrequests";
					}
					else if ($scope.Redirect == 3)
					{
						window.location.href = "#/app/main";
					}  					
					else
					{
						window.location.href = "#/app/shops";
					}
				}
			});
		}
	}
	
	$scope.updateBasket = function()
	{
		//$scope.BasketPrice  = 0;
		
		console.log("basket array: ",$rootScope.BasketArray)
		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}

	$scope.updateBasket();		
	
})


.controller('MainCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,BasketService) {
	
	$scope.BasketPrice = 0;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	
	$scope.updateBasket = function()
	{
		//$scope.BasketPrice  = 0;
		
		console.log("basket array: ",$rootScope.BasketArray)
		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}

	$scope.updateBasket();	
	
	
})


.controller('ProductsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,BasketService) {

  $scope.$on('$ionicView.enter', function(e) {
	
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.host = $rootScope.Host;
	$scope.itemBasket = [];
	$scope.BasketPrice = 0;
	$scope.ItemId = $stateParams.ItemId;
	

	if ($scope.ItemId == -1)
		$scope.ProductsNewArray = $rootScope.ProductsArray;
	else
		$scope.ProductsNewArray = $rootScope.superMarketProducts[$scope.ItemId].products;

	console.log("new products:",$rootScope.ProductsArray)


	
	$scope.changeCheckBox = function(index,item,state)
	{
		if (state == 1)
		{
			item.isClicked = 0;
			item.quan = "1";
		}		
		else
		{
			item.isClicked = 1;
			//$scope.updateBasket();
		}
		console.log("basket new : ", $rootScope.BasketArray)	
		
		$scope.AddBasket(index,item);
		
	}
	
	$scope.changeCheckBox2 = function(index,item,state)
	{
		if (item.isClicked == 0)
		{
			item.isClicked = 1;
			$scope.AddBasket(index,item);
		}
	}	
	
	
	
	$scope.AddBasket = function(index,item)
	{
		
		//$scope.itemBasket = [];
		$scope.BasketPrice = 0;
		if ($rootScope.BasketArray.length == 0)
		{
			$rootScope.BasketArray = new Array();
			//alert ("empty array");
		}
		
		if (item.isClicked == 1)
		{
			$rootScope.BasketArray.push(item);
		}
		else
		{
			item.isClicked = 0;
			for(var i=0;i< $scope.BasketArray.length;i++)
			{
				if ($scope.BasketArray[i].index == item.index)
				{
					$scope.BasketArray.splice(i, 1);
				}
			}
			
		}
		
		

		console.log("array1 new: ", $rootScope.BasketArray);
		

		$scope.updateBasket();

		
		console.log("scope basket array: ", $rootScope.BasketArray)
		console.log("------------------------------");

	}



	$scope.updateBasket = function()
	{
		
		$scope.BasketPrice  = 0;
		
		console.log("BasketArray: " , $rootScope.BasketArray,$rootScope.BasketArray.length)
		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
				console.log("basket price: ",$rootScope.BasketArray[i].new_price)
		}
		
	}

	$scope.updateBasket();

	
	$scope.CartOptions = function() 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'מוצר נוסף לעגלה בהצלחה , האם ברצונך להמשיך לסל קניות?',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'סל קניות',
		type: 'button-positive',
		onTap: function(e) { 
		  window.location.href = "#/app/orderform";
		}
	   },
	   {
		text: 'המשך בקניות',
		type: 'button-calm',
		onTap: function(e) { 
		 
		}

	   },
	   ]
	  });
	
	};	
	
	
	
	$scope.CartAdd = function()
	{
		//$rootScope.BasketArray = $scope.itemBasket;
		console.log($rootScope.BasketArray);
		//$scope.CartOptions();
		window.location.href = "#/app/orderform";
		/*
			$ionicPopup.alert({
			 title: 'חבילה נוספה לסל בהצלחה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		 */
	}
	
	$scope.goToBasket = function()
	{
		console.log('basket: ', $scope.itemBasket);
		
		if ($scope.BasketPrice == 0)
		{
			$ionicPopup.alert({
			 title: 'יש תחילה להוסיף מוצרים לסל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });	
		}
		else
		{
			window.location.href = "#/app/basket";
		}
	}

	
	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}


	

});
	
	
})


.controller('BasketCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.host = $rootScope.Host;
	$scope.BasketPrice = 0;
	console.log("Barray : " ,$rootScope.BasketArray )
	$scope.BasketArray = $rootScope.BasketArray;
    $scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';


	for(var i=0;i< $scope.BasketArray.length;i++)
	{
			$scope.BasketArray[i].isClicked = "1";
	}
		
		
	$scope.checkoutBtn = function()
	{
		if ($scope.BasketArray.length == 0)
		{
			$ionicPopup.alert({
			 title: 'סל קניות ריק יש לבחור תחילה מוצרים לקניה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else
		{
			window.location.href = "#/app/orderform";	
		}
		
		
	}
		

	$scope.deleteProduct = function(index)
	{
		$scope.BasketArray[index].isClicked = false;
		$scope.BasketArray.splice(index, 1);
		$scope.updateBasket();
		//console.log($scope.ProductsArray);
	}
	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}	

	
	$scope.updateBasket = function()
	{
		$scope.BasketPrice = 0;
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}

	$scope.updateBasket();
	
	$scope.changeCheckBox = function(index,item,type)
	{
		var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקת מוצר מהעגלה?',
		   cancelText: 'ביטול',
		     okText: 'אישור', 

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$scope.BasketArray[index].quan = "1";
			$scope.BasketArray[index].isClicked = "0";
			$scope.BasketArray.splice(index, 1);
			$scope.updateBasket();
		 } 
	   });
	   
	}
	
	$scope.AddBasket = function(index,item)
	{
		$scope.BasketArray[index].quan = item.quan;
		$scope.updateBasket();

	}

	$scope.checkExtras= function(item)
	{
		if (item)
		{
			$scope.CountSelected = 0;
			for(var i=0;i< item.length;i++)
			{
				if (item[i].isClicked == 1)
					$scope.CountSelected++;
			}	
			
			if ($scope.CountSelected > 0)
				return true;
			else
				return false;			
		}

	}
	
	$scope.extrasCount= function(item)
	{
		if (item)
		{
			$scope.CountSelected2 = 0;
			for(var i=0;i< item.length;i++)
			{
				if (item[i].isClicked == 1)
					$scope.CountSelected2++;
			}	
			if ($scope.CountSelected2 > 0)
				return $scope.CountSelected2
			else
				return $scope.CountSelected2;	
		}
	}	


	
	$scope.showDetails = function()
	{
		console.log("basket: ",$scope.BasketArray)
		
	}

	
})

.controller('ShopsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.host = $rootScope.Host;
	$scope.BasketPrice = 0;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';

	$scope.StoresArray = $rootScope.storesArray;
	
	

	$scope.updateBasket = function()
	{
		//$scope.BasketPrice  = 0;
		
		console.log("basket array: ",$rootScope.BasketArray)
		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}

	$scope.updateBasket();	
	

})

.controller('StoreMenuCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$timeout) {
	$scope.$on('$ionicView.enter', function(e) {
		 $timeout(function() {
	$scope.BasketPrice  = 0;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';

	$scope.host = $rootScope.Host;
	$scope.MenuId = $stateParams.ItemId;
	$scope.menuArray = $rootScope.storesArray[$scope.MenuId].products;
	//console.log("menu: ",$scope.menuArray)
	
	

	
	$scope.checkExtras= function(item)
	{
		if (item)
		{
			$scope.CountSelected = 0;
			for(var i=0;i< item.length;i++)
			{
				if (item[i].isClicked == 1)
					$scope.CountSelected++;
			}	
			
			if ($scope.CountSelected > 0)
				return true;
			else
				return false;			
		}

	}

	$scope.extrasCount= function(item)
	{
		if (item)
		{
			$scope.CountSelected2 = 0;
			for(var i=0;i< item.length;i++)
			{
				if (item[i].isClicked == 1)
					$scope.CountSelected2++;
			}	
			if ($scope.CountSelected2 > 0)
				return $scope.CountSelected2
			else
				return $scope.CountSelected2;	
		}
	}

	
	for(var i=0;i< $scope.menuArray.length;i++)
	{
		$scope.menuArray[i].quan = "1";
		$scope.menuArray[i].isClicked = 0;
		$scope.menuFlag = 0;
		
		for(var j=0;j< $rootScope.BasketArray.length;j++)
		{	
			if ($rootScope.BasketArray[j].index == $scope.menuArray[i].index)
			{
				$scope.menuFlag = 1;
			}
		}
		
			if ($scope.menuFlag)
			{
				$scope.menuArray[i].isClicked = 1;
			}
		
		/*
			if ($scope.menuArray[i].isClicked == 1)
				$scope.menuArray[i].isClicked = 1;
			else
				$scope.menuArray[i].isClicked = 0;
		*/
	}	

	$scope.AddItem = function(index,item)
	{
		if (item.extras)
		{
			$state.go('app.extras', { StoreId: $scope.MenuId, ProductId: index });
		}
		else
		{
			if (item.isClicked == 0)
			{
				item.isClicked = 1;
				$scope.AddBasket(index,item);

				$ionicPopup.alert({
				 title: item.name+ ' הוסף לעגלה',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });				
			}
		}
	}
		
		
	$scope.changeCheckBox = function(index,item,state)
	{
		if (state == 1)
		{
			item.isClicked = 0;
			item.quan = "1";
		}		
		else
		{
			item.isClicked = 1;
			
			if(item.extras)
			$state.go('app.extras', { StoreId: $scope.MenuId, ProductId: index });
			//window.location.href = "#/app/extras/"+$scope.MenuId+"/"+$scope.menuArray;
		}
			
		$scope.AddBasket(index,item);
	}



	$scope.AddBasket = function(index,item)
	{
		
		//$scope.itemBasket = [];
		$scope.BasketPrice = 0;
		$rootScope.BasketArray = new Array();
		
		for(var i=0;i< $scope.menuArray.length;i++)
		{
		
			if ($scope.menuArray[i].isClicked == 1)
			{
				$rootScope.BasketArray.push($scope.menuArray[i]);
				$scope.menuArray[i].isClicked = 1;
			}			
			else
			{
				$scope.menuArray[i].isClicked = 0;
			}
		}

		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
		   $scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		   console.log($rootScope.BasketArray[i].new_price+ ' : ' + $scope.BasketPrice)
		}		
		
		//$rootScope.BasketArray = new Array();
		//$rootScope.BasketArray = $scope.itemBasket;
		//console.log("basket array: ", $rootScope.BasketArray);
		
		console.log("scope basket array: ", $rootScope.BasketArray)
		console.log("------------------------------");

		//console.log(item);
		//alert (444);
	}

	
	
	$scope.updateBasket = function()
	{	
		//console.log("basket array: ",$rootScope.BasketArray)
		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
		
		
	}

	$scope.updateBasket();

	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}
	
	
	console.log("basket : ", $rootScope.BasketArray)

	})
	   }, 1000);
})


.controller('ExtrasCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';

	$scope.StoreId = $stateParams.StoreId;
	$scope.ProductId = $stateParams.ProductId;
	$scope.BasketPrice = 0;
	$scope.selectedExtras = [];

	if($rootScope.storesArray[$scope.StoreId])
	$scope.extrasArray = $rootScope.storesArray[$scope.StoreId].products[$scope.ProductId].extras;
	else
	$scope.extrasArray = [];
	
	$scope.ProductArray = $rootScope.storesArray[$scope.StoreId].products[$scope.ProductId];
	$scope.ProductId = $scope.ProductArray.index;	
	//console.log("extras: " , $scope.extrasArray)
	
	//console.log("extras1 : " , $scope.extrasArray)
	
	
	//if (!$scope.extrasArray[0].isClicked)
	//{
		
		for(var i=0;i< $scope.extrasArray.length;i++)
		{
			$scope.extrasArray[i].isClicked = 0;
		}	
		
	//}
	
	
	//console.log("extras2 : " , $scope.extrasArray)
	

	$scope.AddBasket = function(item)
	{
		//$scope.itemBasket = [];
		//$rootScope.BasketArray = new Array();
		$scope.BasketPrice = 0;
		$rootScope.BasketArray.push(item);
	}
	
	$scope.changeCheckBox = function(index,item,state)
	{
		if (state == 1)
		{
			item.isClicked = 0;
			//item.quan = "1";
		}		
		else
		{
			item.isClicked = 1;
		}
			
		//$scope.AddBasket(index,item);
	}

	$scope.saveExtras = function()
	{


		if ($scope.ProductArray.index == $scope.ProductId)
		{
			$scope.ProductArray.isClicked = 1;
			$scope.AddBasket($scope.ProductArray);
			$scope.updateBasket();
			//alert (555);
		}

			
		window.location.href = "#/app/storemenu/"+$scope.StoreId;

	}


	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();

	

})


.controller('OrderFormCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$timeout) {
  $scope.$on('$ionicView.enter', function(e) {
	   $timeout(function() {
      
    
	$scope.OrderTime = 0;
	$scope.InviteDateImage1 = 'img/late2.png';
	$scope.InviteDateImage2 = 'img/now1.png';	
	$scope.payAmount = 0;
	$scope.BasketPrice = 0;
	$scope.timeStamp = Date.now(); 
	$scope.BasketPrice = 0; 
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.DeliveryPrice = 15;
	$scope.TotalPrice = 0;
	
	var myDate = new Date();
	var month = myDate.getMonth() + 1;
	var day = myDate.getDate();
	
	if(day<10) {
		day='0'+day
	} 
	if(month<10) {
		month='0'+month
	} 
	//.toString().substr(2,2);
	$scope.prettyDate =(day +'-'+ month) +'-'+ myDate.getFullYear();

	
	$scope.fields = 
	{
		"pickup" : "",
		"dest" : $localStorage.destinationName,
		//"dest" : $localStorage.address,
		"deliverytype" : "",
		"remarks" : "",
		"deliverydate" : $scope.prettyDate
	}

	
	$scope.$watch($scope.fields.pickup.id, function(value) 
	 {
		 if ($scope.fields.pickup.id)
		 {
//			 alert (55);
		 }
	 });

	$scope.autocompleteOptions = {
		componentRestrictions: { country: 'il' },
		//types: ['geocode']
	}

	$scope.pickupData = function()
	{
		console.log("pickup: ",$scope.fields.pickup);
		
		
		if ($scope.fields.pickup.id)
		{
			if ($scope.fields.pickup.geometry)
			{
				if ($scope.fields.pickup.geometry.access_points)
				{
					alert ("pickup lat : "+$scope.fields.pickup.geometry.access_points[0].location.lat+ " pickup lng: "+ $scope.fields.pickup.geometry.access_points[0].location.lng)
				}

			}	
		}
		else
		{
			$scope.fields.pickup = '';
		}		
	}

	$scope.destData = function()
	{
		console.log("dest: ",$scope.fields.dest);
		
		if ($scope.fields.dest.id)
		{
			if ($scope.fields.dest.geometry)
			{
				if ($scope.fields.dest.geometry.access_points)
				{
					alert ("dest lat : "+$scope.fields.dest.geometry.access_points[0].location.lat +" dest lng: "+ $scope.fields.dest.geometry.access_points[0].location.lng)
				}	

			}	
		}
		else
		{
			$scope.fields.dest = '';
		}		
	}

	
	$scope.ChangeDate = function(type)
	{
		$scope.OrderTime = type;
		
		if (type == 0)
		{
			$scope.InviteDateImage1 = 'img/late2.png';
			$scope.InviteDateImage2 = 'img/now1.png';			
		}
		else
		{
			$scope.InviteDateImage1 = 'img/late1.png';
			$scope.InviteDateImage2 = 'img/now2.png';			
		}
	}
	
	$scope.loginOptions = function() 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'יש להתחבר או להרשם כדי להשלים הזמנה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'הרשמה',
		type: 'button-positive',
		onTap: function(e) { 
		  window.location.href = "#/app/register/1";
		}
	   },
	   {
		text: 'התחברות',
		type: 'button-calm',
		onTap: function(e) { 
		  window.location.href = "#/app/login/1";
		 
		}

	   },
	   	   {
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },	   
	   ]
	  });
	
	};	

	$scope.completeOrder = function()
	{

		if (!$localStorage.userid)
		{
			$scope.loginOptions();
		}
		
		else 
		{
			/*
			if ($scope.fields.dest =="")
			{
				$ionicPopup.alert({
				 title: 'יש למלא כתובת למשלוח',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });				
			}
			*/
			 if ($rootScope.BasketArray.length == 0)
			{
				$ionicPopup.alert({
				 title: 'סל קניות ריק יש לבחור מוצרים לקניה לפני תשלום',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });					
			}

			else
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				console.log("basket array ", $rootScope.BasketArray)
				//return;
				if ($rootScope.basketId =="")
					$rootScope.basketId = $scope.timeStamp;

				
				/*
					if ($scope.fields.dest.formatted_address)
					{
						$scope.newaddress = String($scope.fields.dest.geometry.location);
						$scope.split = $scope.newaddress.split(",");
						$scope.locationx = $scope.split[0].replace("(", "");
						$scope.locationy = $scope.split[1].replace(")", "");						
					}
				*/

		
					//alert ($rootScope.basketId)
					checkout_data = 
					{
						"user" : $localStorage.userid,
						"pickup" : $scope.fields.pickup,
						"dest" : $localStorage.destinationName,
						//"dest" : $scope.fields.dest.formatted_address,
						"location_lat" : $localStorage.destinationtLat,
						"location_lng" : $localStorage.destinationLng,
						
						//"location_lat" : $scope.locationx,
						//"location_lng" : $scope.locationy,
						"marketid" : $localStorage.marketId,
						"deliverytype" : $scope.fields.deliverytype,
						"remarks" : $scope.fields.remarks,
						"deliverydate" : $scope.fields.deliverydate,
						"sum" : $scope.BasketPrice,
						"products" : $rootScope.BasketArray,
						"cartId" : $rootScope.basketId

					}					
					$http.post($rootScope.Host+'/checkout_cart.php',checkout_data).success(function(data)
					{
							$rootScope.basketId = '';
							$scope.BasketPrice = 0;
							$scope.payAmount = 0;
							$scope.clearSelected();
							$rootScope.BasketArray = [];
							//$rootScope.BasketArray = new Array();
							$scope.fields.pickup = '';
							$scope.fields.dest = '';
							$scope.fields.deliverytype = '';
							$scope.fields.remarks = '';
							$scope.fields.deliverydate = '';

						  
						   $scope.updateBasket();
						   
						   
						   if (data.response.orderid)
						   {
							   window.location.href = "#/app/complete/"+data.response.orderid;
						   }
						   else
						   {
							
								$ionicPopup.alert({
								 title: 'הזמנתך התקבלה בהצלחה - תוכל לצפות בה במסך הזמנות האישי',
								buttons: [{
									text: 'אשר',
									type: 'button-positive',
								  }]
							   });	
						   							   
						   }
						   
						   
					});			
			}			
		}

		


	}
	
	$scope.updateBasket = function()
	{
		$scope.BasketPrice = 0;
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
				$scope.TotalPrice = $scope.DeliveryPrice+$scope.BasketPrice;
		
	}

	$scope.updateBasket();

	$scope.clearSelected = function()
	{
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$rootScope.BasketArray[i].isClicked = false;
		}			
	}
	    }, 1000);
});
})

.controller('ContactCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	
	$scope.fields = 
	{
		"name" : $localStorage.name,
		"phone" : $localStorage.phone,
		"email" : $localStorage.email,
		"remarks" : ""
	}
	
	$scope.sendContact = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailregex = /\S+@\S+\.\S+/;


		if ($scope.fields.name =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא שם מלא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}

		
		else if ($scope.fields.phone =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא מספר טלפון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.fields.email =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מייל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}
		
		else if (!emailregex.test($scope.fields.email))
		{
			$ionicPopup.alert({
			 title: 'מייל לא תקין יש לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });				
		}		
		else if ($scope.fields.remarks =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא תוכן הפנייה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
		}

		else
		{
			contact_data = 
			{
				"name" : $scope.fields.name,
				"phone" : $scope.fields.phone,
				"email" : $scope.fields.email,
				"remarks" : $scope.fields.remarks,
				"send" : 1 
			}					
			$http.post($rootScope.Host+'/contact.php',contact_data).success(function(data)
			{
					$ionicPopup.alert({
					 title: 'תודה '+ $scope.fields.name+ ' פניתך התקבלה בהצלחה , נחזור אליך בהקדם.',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });	
				   
				   $scope.fields.name = '';
				   $scope.fields.phone = '';
				   $scope.fields.email = '';
				   $scope.fields.remarks = '';
				   
				   window.location.href = "#/app/main";
			});			
		}		
	}

	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();	

})	

.controller('AboutCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.AboutText = $rootScope.SettingsArray[0].about_text;

	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();
	
})

.controller('SpecialRequestsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.OrderTime = 0;
	$scope.DeliveryPrice = 15;
	$scope.TotalPrice = 0;

	var myDate = new Date();
	var month = myDate.getMonth() + 1;
	var day = myDate.getDate();
	
	if(day<10) {
		day='0'+day
	} 
	if(month<10) {
		month='0'+month
	} 
	//.toString().substr(2,2);
	$scope.prettyDate =(day +'-'+ month) +'-'+ myDate.getFullYear();
	
	
	
	$scope.fields = 
	{
		"pickup" : "",
		"dest" : "",
		"subject" : "",
		"remarks" : "",
		"deliverydate" : $scope.prettyDate
	}
	
	$scope.InviteDateImage1 = 'img/late2.png';
	$scope.InviteDateImage2 = 'img/now1.png';
	
	$scope.ChangeDate = function(type)
	{
		$scope.OrderTime = type;
		
		if (type == 0)
		{
			$scope.InviteDateImage1 = 'img/late2.png';
			$scope.InviteDateImage2 = 'img/now1.png';			
		}
		else
		{
			$scope.InviteDateImage1 = 'img/late1.png';
			$scope.InviteDateImage2 = 'img/now2.png';			
		}
	}
	
	$scope.loginOptions = function() 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'יש להתחבר או להרשם כדי להשלים הזמנה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'הרשמה',
		type: 'button-positive',
		onTap: function(e) { 
		  window.location.href = "#/app/register/2";
		}
	   },
	   {
		text: 'התחברות',
		type: 'button-calm',
		onTap: function(e) { 
		  window.location.href = "#/app/login/2";
		 
		}

	   },
	   	   {
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },	   
	   ]
	  });
	
	};	

	
	$scope.sendRequest = function()
	{
		if (!$localStorage.userid)
		{
			$scope.loginOptions();
		}
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
			if ($scope.fields.pickup =="")
			{
				$ionicPopup.alert({
				 title: 'יש למלא כתובת איסוף',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });				
			}

			
			else if ($scope.fields.dest =="")
			{
				$ionicPopup.alert({
				 title: 'יש למלא כתובת פיזור',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });				
			}
			/*
			else if ($scope.fields.subject =="")
			{
				$ionicPopup.alert({
				 title: 'יש למלא אופן הבקשה',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });				
			}
			*/
			
		
			else if ($scope.fields.remarks =="")
			{
				$ionicPopup.alert({
				 title: 'יש למלא הערות',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });
			}

			else
			{
				$scope.pickup = $scope.fields.pickup.formatted_address;
				$scope.dest = $scope.fields.dest.formatted_address;
				
				request_data = 
				{
					"user" : $localStorage.userid,
					"name" : $localStorage.name,
					"phone" : $localStorage.phone,
					"email" : $localStorage.email,				
					"pickup" : $scope.pickup,
					"dest" : $scope.dest,
					"deliverydate" : $scope.fields.deliverydate,
					"subject" : $scope.fields.subject,
					"remarks" : $scope.fields.remarks,
					"send" : 1 
				}					
				$http.post($rootScope.Host+'/send_request.php',request_data).success(function(data)
				{
						$ionicPopup.alert({
						 title: 'תודה '+ $localStorage.name+ ' פניתך התקבלה בהצלחה , נחזור אליך בהקדם.',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });	
					   
					   $scope.fields.pickup = '';
					   $scope.fields.dest = '';
					   $scope.fields.subject = '';
					   $scope.fields.remarks = '';
					   
					   window.location.href = "#/app/main";
				});			
			}			
		}
			
	}
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	

			$scope.TotalPrice = $scope.DeliveryPrice+$scope.BasketPrice;

	}

	$scope.updateBasket();

})


.controller('DetailsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.TypeId = $stateParams.Type;
	$scope.ProductId = $stateParams.ItemId;
	$scope.BasketPrice = 0;

	
	for(var i=0;i< $rootScope.ProductsArray.length;i++)
	{
		if ($scope.ProductId == $rootScope.ProductsArray[i].index)
		{
			$scope.ProductData = $rootScope.ProductsArray[i];
			//alert ($scope.ProductData.quan)
			//$scope.ProductData.quan = "1";
			
			console.log("ProductData" , $scope.ProductData)
		}
	}
	
	
		
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}

	$scope.addCart = function(item)
	{
		if (item.isClicked == 0)
		{
			item.isClicked = 1;
			
			$scope.BasketPrice = 0;
			if ($rootScope.BasketArray.length == 0)
			{
				$rootScope.BasketArray = new Array();
			}
			
			if (item.isClicked == 1)
			{
				$rootScope.BasketArray.push(item);
			}
			
			$scope.updateBasket();
			
			window.location.href = "#/app/"+$scope.TypeId;
		}

		
		
	}
	
	$scope.updateBasket = function(item)
	{
		if (item)
		{
			//alert ($scope.ProductData.quan);
		}
		$scope.BasketPrice  = 0;
		
		console.log("BasketArray: " , $rootScope.BasketArray,$rootScope.BasketArray.length)
		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
				console.log("basket price: ",$rootScope.BasketArray[i].new_price)
		}
		
	}

	$scope.updateBasket();	
	
})

.controller('PackageFormCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	
	$scope.OrderTime = 0;
	$scope.InviteDateImage1 = 'img/late2.png';
	$scope.InviteDateImage2 = 'img/now1.png';
	$scope.DeliveryPrice = 15;
	$scope.TotalPrice = 0;
	
	$scope.timeStamp = Date.now(); 

	var myDate = new Date();
	var month = myDate.getMonth() + 1;
	var day = myDate.getDate();
	
	if(day<10) {
		day='0'+day
	} 
	if(month<10) {
		month='0'+month
	} 
	//.toString().substr(2,2);
	$scope.prettyDate =(day +'-'+ month) +'-'+ myDate.getFullYear();
	

	$scope.fields = 
	{
		"pickup" : "",
		"dest" : $localStorage.address,
		"deliverytype" : "",
		"packagesize" : 6,
		"remarks" : "",
		"deliverydate" : $scope.prettyDate
	}

	
	$scope.$watch($scope.fields.pickup.id, function(value) 
	 {
		 if ($scope.fields.pickup.id)
		 {
//			 alert (55);
		 }
	 });

	$scope.autocompleteOptions = {
		componentRestrictions: { country: 'il' },
		bounds: hyderabadBounds,
  		types:  ['establishment [or whatever fits your usage]']
	}

	$scope.pickupData = function()
	{
		console.log("pickup: ",$scope.fields.pickup);
		
		
		if ($scope.fields.pickup.id)
		{
			if ($scope.fields.pickup.geometry)
			{
				if ($scope.fields.pickup.geometry.access_points)
				{
					alert ("pickup lat : "+$scope.fields.pickup.geometry.access_points[0].location.lat+ " pickup lng: "+ $scope.fields.pickup.geometry.access_points[0].location.lng)
				}	
			}	
		}
	}

	$scope.destData = function()
	{
		console.log("dest: ",$scope.fields.dest);
		
		
		if ($scope.fields.dest.id)
		{
			if ($scope.fields.dest.geometry)
			{
				if ($scope.fields.dest.geometry.access_points)
				{
					alert ("dest lat : "+$scope.fields.dest.geometry.access_points[0].location.lat +" dest lng: "+ $scope.fields.dest.geometry.access_points[0].location.lng)
				}	
			}	
		}
	}

	
	$scope.ChangeDate = function(type)
	{
		$scope.OrderTime = type;
		
		if (type == 0)
		{
			$scope.InviteDateImage1 = 'img/late2.png';
			$scope.InviteDateImage2 = 'img/now1.png';			
		}
		else
		{
			$scope.InviteDateImage1 = 'img/late1.png';
			$scope.InviteDateImage2 = 'img/now2.png';			
		}
	}
	
	$scope.getPackageSizes = function()
	{
		
			$http.get($rootScope.Host+'/get_Packages.php').success(function(data)
			{
				$scope.packagesArray = data;
				//$rootScope.UsersArray = $scope.AdminOrders;
				//console.log("users : " , $scope.UsersArray)
			});		
	}
	
	$scope.getPackageSizes();	
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
		
		$scope.TotalPrice = $scope.DeliveryPrice+$scope.BasketPrice;
	}

	$scope.updateBasket();

	

})
.controller('OrdersCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.ActiveTab = 0;
	
	$scope.getUserOrders = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
			orders_data = 
			{
				"user" : $localStorage.userid
			}					
			
			$http.post($rootScope.Host+'/get_Orders.php',orders_data).success(function(data)
			{
				$scope.OrdersArray = data;
				$rootScope.UserOrders = $scope.OrdersArray;
				console.log("orders: " , $scope.OrdersArray)
			});		
	}
	
	$scope.getUserOrders();
	
	$scope.setActiveTab = function(type)
	{
		$scope.ActiveTab = type;
	}
	
	$scope.OrderStatus = function(type)
	{
		if (type == 0)
		{
			$scope.FormattedStatus = 'הזמנה חדשה';
		}
		if (type == 1)
		{
			$scope.FormattedStatus = 'הזמנה בטיפול';
		}
		if (type == 2)
		{
			$scope.FormattedStatus = 'הזמנה נשלחה';
		}
		if (type == 3)
		{
			$scope.FormattedStatus = 'הזמנה הושלמה';
		}		
		
		return $scope.FormattedStatus;	
	}
	
	$scope.FormattedDate = function(date)
	{
		$scope.split = date.split(" ");
		$scope.splitHour = $scope.split[1].split(":");
		$scope.DateFormat = $scope.split[0];
		$scope.splitDate = $scope.DateFormat.split("-");
		$scope.newDate = $scope.splitDate[2]+'/'+$scope.splitDate[1]+'/'+$scope.splitDate[0];
		$scope.newHour = $scope.splitHour[0]+':'+$scope.splitHour[1];
		
		return $scope.newDate+ ' '+  $scope.newHour2;

	}
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();

	

})

.controller('OrderDetailsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.ActiveTab = 0;
	$scope.orderIndex = $stateParams.ItemId;

	
	
	$scope.orderData = $rootScope.UserOrders[$scope.orderIndex];
	$scope.CartProducts = $scope.orderData.cart;
	
	console.log("order details: " , $scope.orderData)

	$scope.FormattedDate = function(date)
	{
		$scope.split = date.split(" ");
		$scope.splitHour = $scope.split[1].split(":");
		$scope.DateFormat = $scope.split[0];
		$scope.splitDate = $scope.DateFormat.split("-");
		$scope.newDate = $scope.splitDate[2]+'/'+$scope.splitDate[1]+'/'+$scope.splitDate[0];
		$scope.newHour = $scope.splitHour[0]+':'+$scope.splitHour[1];
		
		return $scope.newDate+ ' '+  $scope.newHour;

	}
	$scope.OrderStatus = function(type)
	{
		if (type == 0)
		{
			$scope.FormattedStatus = 'הזמנה חדשה';
		}
		if (type == 1)
		{
			$scope.FormattedStatus = 'הזמנה בטיפול';
		}
		if (type == 2)
		{
			$scope.FormattedStatus = 'הזמנה נשלחה';
		}
		if (type == 3)
		{
			$scope.FormattedStatus = 'הזמנה הושלמה';
		}		
		
		return $scope.FormattedStatus;	
	}

	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}

	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();

	
})

.controller('AdminOrdersCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {


	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.ActiveTab = 0;


	$scope.getUsers = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
			users_data = 
			{
				"user" : $localStorage.userid
			}					
			
			$http.post($rootScope.Host+'/get_Users.php',users_data).success(function(data)
			{
				$scope.UsersArray = data;
				//$rootScope.UsersArray = $scope.AdminOrders;
				//console.log("users : " , $scope.UsersArray)
			});		
	}
	
	$scope.getUsers();

	
	
	$scope.getAdminOrders = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
			orders_data = 
			{
				"user" : $localStorage.userid
			}					
			
			$http.post($rootScope.Host+'/get_Admin_Orders.php',orders_data).success(function(data)
			{
				$scope.AdminOrders = data;
				$rootScope.AdminOrdersArray = $scope.AdminOrders;
				
				
				for(var i=0;i< $scope.AdminOrders.length;i++)
				{
					$scope.AdminOrders[i].delivery = "0";
				}	

		
				console.log("admin orders: " , $scope.AdminOrders)
			});		
	}
	
	$scope.getAdminOrders();
	
	
	$scope.sendOrder = function(item,index)
	{
		$scope.deliveryName = '';
		
		for(var i=0;i< $scope.UsersArray.length;i++)
		{
			if ($scope.UsersArray[i].id == item.delivery)
			{
				$scope.deliveryName = $scope.UsersArray[i].name;
			}
		}	
		
		var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר העברת הזמנה מספר '+item.index+ ' לשליח בשם: '+$scope.deliveryName+' ?',
		   cancelText: 'ביטול',
		     okText: 'אישור', 		 

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			 
			send_data = 
			{
				"user" : $localStorage.userid,
				"order_id" : item.index,
				"deliver_id" :  item.delivery,
				"send" : 1
			}					
			console.log("send : " , send_data)
			$http.post($rootScope.Host+'/send_order.php',send_data).success(function(data)
			{
				$ionicPopup.alert({
				 title: 'תודה '+ $localStorage.name+ ' הזמנה נשלחה בהצלחה לשליח',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });			
					
				item.status = 1;
				//$scope.AdminOrders[index].status = "1";
				  
			});	
		 } 
	   });

				
		//alert (item.delivery)
		console.log("order : ", item)
	}
	
	
	$scope.setActiveTab = function(type)
	{
		$scope.ActiveTab = type;
	}
	
	$scope.OrderStatus = function(type)
	{
		if (type == 0)
		{
			$scope.FormattedStatus = 'הזמנה חדשה';
		}
		if (type == 1)
		{
			$scope.FormattedStatus = 'הזמנה בטיפול';
		}
		if (type == 2)
		{
			$scope.FormattedStatus = 'הזמנה נשלחה';
		}
		if (type == 3)
		{
			$scope.FormattedStatus = 'הזמנה הושלמה';
		}		
		
		return $scope.FormattedStatus;	
	}
	
	$scope.FormattedDate = function(date)
	{
		$scope.split = date.split(" ");
		$scope.splitHour = $scope.split[1].split(":");
		$scope.DateFormat = $scope.split[0];
		$scope.splitDate = $scope.DateFormat.split("-");
		$scope.newDate = $scope.splitDate[2]+'/'+$scope.splitDate[1]+'/'+$scope.splitDate[0];
		$scope.newHour = $scope.splitHour[0]+':'+$scope.splitHour[1];
		
		return $scope.newDate+ ' '+  $scope.newHour;

	}
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();
	
	

})

.controller('DeliveryCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.ActiveTab = 0;



	
	$scope.getAdminOrders = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
			orders_data = 
			{
				"user" : $localStorage.userid
			}					
			
			$http.post($rootScope.Host+'/get_Delivery_Orders.php',orders_data).success(function(data)
			{
				$scope.deliveryOrders = data;
				$rootScope.deliveryOrdersArray = $scope.AdminOrders;
				
				/*
				for(var i=0;i< $scope.deliveryOrders.length;i++)
				{
					$scope.AdminOrders[i].delivery = "0";
				}	
				*/

		
				console.log("delivery orders: " , $scope.deliveryOrders)
			});		
	}
	
	$scope.getAdminOrders();
	
	
	$scope.orderRecived = function(item,index)
	{
		var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מסירת הזמנה מספר '+item.order[0].index+' ?',
		   cancelText: 'ביטול',
		     okText: 'אישור', 		 

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			 
			send_data = 
			{
				"user" : $localStorage.userid,
				"order_id" : item.order[0].index,
				//"deliver_id" :  item.delivery,
				"send" : 1
			}					
			console.log("send : " , send_data)
			$http.post($rootScope.Host+'/update_order.php',send_data).success(function(data)
			{
				$ionicPopup.alert({
				 title: 'תודה '+ $localStorage.name+ ' הזמנה עודכנה בהצלחה',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });			
					
				item.status = 1;
				//$scope.AdminOrders[index].status = "1";
				  
			});	
			
			item.order[0].status = 3;
		 } 
	   });
		
	}

	
	$scope.setActiveTab = function(type)
	{
		$scope.ActiveTab = type;
	}
	
	$scope.OrderStatus = function(type)
	{
		if (type == 0)
		{
			$scope.FormattedStatus = 'הזמנה חדשה';
		}
		if (type == 1)
		{
			$scope.FormattedStatus = 'הזמנה בטיפול';
		}
		if (type == 2)
		{
			$scope.FormattedStatus = 'הזמנה נשלחה';
		}
		if (type == 3)
		{
			$scope.FormattedStatus = 'הזמנה הושלמה';
		}		
		
		return $scope.FormattedStatus;	
	}
	
	$scope.FormattedDate = function(date)
	{
		$scope.split = date.split(" ");
		$scope.splitHour = $scope.split[1].split(":");
		$scope.DateFormat = $scope.split[0];
		$scope.splitDate = $scope.DateFormat.split("-");
		$scope.newDate = $scope.splitDate[2]+'/'+$scope.splitDate[1]+'/'+$scope.splitDate[0];
		$scope.newHour = $scope.splitHour[0]+':'+$scope.splitHour[1];
		
		return $scope.newDate+ ' '+  $scope.newHour;

	}
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();
	
	

})

.controller('FastFoodCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.fields = 
	{
		"dest" : $localStorage.savedAdress 
	}
	
	
	if (!$localStorage.userid)
		$scope.loginStatus = true;
		else
		$scope.loginStatus = false;
	
	$scope.LoginBtn = function()
	{
		window.location.href = "#/app/login/0";
	}
	
	$scope.shopsBtn = function()
	{
		$localStorage.savedAdress = $scope.fields.dest.formatted_address;
		window.location.href = "#/app/shops";
	}

	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	
	}

	$scope.updateBasket();
	
	

})

.controller('AddressCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$cordovaGeolocation) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	
	$scope.fields = 
	{
		"locationname" : "",
		"location_lat" : "",
		"location_lng" : ""
	}
	
	$localStorage.destinationName = '';
	$localStorage.destinationtLat = '';
	$localStorage.destinationLng = '';	

	$scope.getLocationName = function(lat,lng)
	{
		$http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&sensor=true').then(function(data)
		{   
		console.log("location: ", data);
		//$scope.fields.city = data.data.results[0].address_components[2].long_name;	
		$scope.fields.locationname = data.data.results[0].formatted_address;
		//alert ($scope.fields.locationname)
		//alert ($scope.fields.locationname)
		//alert ($scope.fields)
		}, function(err) {
		  // error
		});		
	}

	
	$scope.getLocation = function()
	{
		
		var posOptions = {timeout: 10000, enableHighAccuracy: false};
		$cordovaGeolocation
		.getCurrentPosition(posOptions)
		.then(function (position) {


		   $scope.fields.location_lat =  position.coords.latitude;
		   $scope.fields.location_lng = position.coords.longitude;
		   
		   
		   $scope.getLocationName($scope.fields.location_lat,$scope.fields.location_lng);

		   
		}, function(err) {
		  // error
		});


	}
	
	
	$scope.getLocation();
	

	$scope.saveAddress = function()
	{
		if ($scope.fields.locationname.formatted_address)
			$localStorage.destinationName = $scope.fields.locationname.formatted_address;
		else
			$localStorage.destinationName = $scope.fields.locationname;
		
		//alert ($localStorage.destinationName)
		
		$localStorage.destinationtLat = $scope.fields.location_lat;
		$localStorage.destinationLng = $scope.fields.location_lng;
		/*
		alert ($localStorage.destinationName)
		alert ($localStorage.destinationtLat)
		alert ($localStorage.destinationLng)
		*/
		window.location.href = "#/app/supermarkets";
	}



})


.controller('SuperMarketsCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$cordovaGeolocation) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	
	$scope.getSuperMarkets = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				
		send_data = 
		{
			"user" : $localStorage.userid,
			//"lat" : 32.830261,
			//"lng" : 35.056773,
			"lat" : $localStorage.destinationtLat,
			"lng" : $localStorage.destinationLng
		}			

		//alert ($localStorage.destinationtLat)
		//alert ($localStorage.destinationLng)
		
		$http.post($rootScope.Host+'/get_supermarkets.php',send_data).success(function(data)
		{
		    console.log("supermarkets : ", data);
			$scope.superMarketsArray = data;
			$rootScope.superMarketProducts = data;
			
			for(var i=0;i< $rootScope.superMarketProducts.length;i++)
			{
				for(var g=0;g< $rootScope.superMarketProducts[i].products.length;g++)
				{
					$rootScope.superMarketProducts[i].products[g].isClicked= false;
					$rootScope.superMarketProducts[i].products[g].quan = "1";

				}

			}

			
				
		});			
	}
	
	$scope.getSuperMarkets();
	
	
	$scope.goProductsPage = function(index,marketid)
	{
		$localStorage.marketId = marketid;
		window.location.href = "#/app/products/"+index;
	}

})


.controller('MapCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$cordovaGeolocation,$timeout) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.ItemId = $stateParams.ItemId;

	$scope.fields =
	{
		"user_locationlat" : "",
		"user_location_lng" : ""
	}

 $scope.locations = [];				
 

 $scope.getOrderDelivers = function()
 {
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	send_data = 
	{
		"user" : $localStorage.userid,
		"orderid" :  $scope.ItemId
	}					
	$http.post($rootScope.Host+'/get_order_map.php',send_data).success(function(data)
	{
		console.log("get_order_map",data);
		if (data.length > 0)
		{
			//alert (data[0].location_lat)
			//alert (data[0].location_lng)
			
			$scope.locations.push({
				"name": "user",
				"lat" : data[0].location_lat,
				"lng" : data[0].location_lng,
				"icon" : "img/blue-dot.png"
			});
			
			
			if (data[0].deliveryman)
			{
				for(var i=0;i< data[0].deliveryman.length;i++)
				{
					if  (data[0].deliveryman[i].location_lat && data[0].deliveryman[i].location_lng)
					{
						$scope.locations.push({
							"name": "deliveryman",
							"personname" : data[0].deliveryman[i].name,
							"lat" : data[0].deliveryman[i].location_lat,
							"lng" : data[0].deliveryman[i].location_lng,
							"icon" : $rootScope.Host+data[0].deliveryman[i].image
						});						
					}

				}
			}
			
			
		}


	});		

			//console.log(data)
			 $timeout(function() 
			 {
				 //google.maps.event.trigger(map, 'resize');
				 //$scope.getUserLocation();
				 //$scope.getOrderDelivers();
		   	}, 3000);

			
 }
 
 $scope.getOrderDelivers();
 
 console.log("use order  location not user current location")
 //alert ($scope.locations);
	


	$scope.setGoogleMap = function(location_lat,location_lng)
	{
		var myLatlng = new google.maps.LatLng(location_lat,location_lng);
		var mapOptions = {
			center: myLatlng,
			zoom: 12,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map"), mapOptions);
		map.setCenter(new google.maps.LatLng(location_lat,location_lng));
		
		 for (i = 0; i < $scope.locations.length; i++) 
		 {  
			//alert ($scope.locations[i].lat)
			var myLocation = new google.maps.Marker({
				position: new google.maps.LatLng($scope.locations[i].lat,$scope.locations[i].lng),
				map: map,
				//http://maps.google.com/mapfiles/ms/icons/blue-dot.png
				
					icon: new google.maps.MarkerImage(
					$scope.locations[i].icon, // my 16x48 sprite with 3 circular icons
					null, // desired size
					null, // offset within the scaled sprite
					null, // anchor point is half of the desired size
					new google.maps.Size(60, 60) // scaled size of the entire sprite
				   ),

				   
				//icon : $scope.locations[i].icon,
				animation: google.maps.Animation.DROP,
				title: $scope.locations[i].personname;
				
				
				
			});		 
		 }
		
		
		
	}	
	
	$scope.setMapMarker = function(lat,lng)
	{
	
	}


		
	$scope.getUserLocation = function()
	{
		
		  var posOptions = {timeout: 10000, enableHighAccuracy: false};
		  $cordovaGeolocation
			.getCurrentPosition(posOptions)
			.then(function (position) {
			  var lat  = position.coords.latitude
			  var long = position.coords.longitude
				  
				$scope.fields.user_locationlat = lat;
				$scope.fields.user_location_lng = long;

				$scope.locations.push({
					"name": "user",
					"lat" : $scope.fields.user_locationlat,
					"lng" : $scope.fields.user_location_lng,
					"icon" : "img/blue-dot.png"
				});

				
				//alert ($scope.fields.location_lat)
				//alert ($scope.fields.location_lng)
				
				$scope.setGoogleMap($scope.fields.user_locationlat,$scope.fields.user_location_lng);
				//$scope.setMapMarker($scope.fields.user_locationlat,$scope.fields.user_location_lng);
				//$scope.getLocationName($scope.fields.location_lat,$scope.fields.location_lng);
			  
			}, function(err) {
			  // error
			});
			
			
			
			
			//$scope.setGoogleMap(32.793887,35.037090);
			
		

			//console.log(data)
			 $timeout(function() 
			 {

		   	}, 300);
		

	}
  
	$scope.getUserLocation();
	






	
})	

.controller('CompleteOrderCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal,$cordovaGeolocation,$timeout) {

	$scope.BasketPrice = 0;
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:120px; margin-left:-10px; margin-top:5px;"/>';
	$scope.OrderId = $stateParams.ItemId;
	
	

})	


	
	
.filter('cutString_TitlePage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' ');
    };
})

.filter('stripslashes', function () {
    return function (value) 
	{
	    return (value + '')
		.replace(/\\(.?)/g, function(s, n1) {
		  switch (n1) {
			case '\\':
			  return '\\';
			case '0':
			  return '\u0000';
			case '':
			  return '';
			default:
			  return n1;
		  }
		});
    };
})
