// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','google.places'])

.run(function($ionicPlatform,$rootScope,$http) {
	
	
	$rootScope.Host = 'http://tapper.co.il/sendit/php/';
	$rootScope.basketId = '';
	$rootScope.storesArray = [];
	$rootScope.BasketArray = [];
	$rootScope.SettingsArray = [];
	$rootScope.UserOrders = [];
	$rootScope.AdminOrdersArray = [];
	$rootScope.deliveryOrdersArray = [];
	$rootScope.savedAdress = '';
	$rootScope.superMarketProducts = [];

	//get products
	$http.get($rootScope.Host+'/getProducts.php').success(function(data)
	{
		$rootScope.ProductsArray = data;
		
			for(var i=0;i< data.length;i++)
			{
				$rootScope.ProductsArray[i].isClicked = false;
				$rootScope.ProductsArray[i].quan = "1";
			}
			
			//$rootScope.BasketArray.push($rootScope.ProductsArray[0])
		//console.log('products: ',$scope.ProductsArray)
	});		

	// get stores
	$http.get($rootScope.Host+'/getStores.php').success(function(data)
	{
		$rootScope.storesArray = data;	
		console.log('stores: ',data);
	});		


	// get settings (about)
	$http.get($rootScope.Host+'/getSettings.php').success(function(data)
	{
		$rootScope.SettingsArray = data;	
		//console.log('SettingsArray: ',$rootScope.SettingsArray);
	});	

	
	
	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	
  $ionicConfigProvider.backButton.previousTitleText(false).text('');

		
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('app.login', {
      url: '/login/:Redirect',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })

    .state('app.forgot', {
      url: '/forgot',
      views: {
        'menuContent': {
          templateUrl: 'templates/forgot.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })
    .state('app.register', {
      url: '/register/:Redirect',
      views: {
        'menuContent': {
          templateUrl: 'templates/register.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })	
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })	
    .state('app.products', {
      url: '/products/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/products.html',
          controller: 'ProductsCtrl'
        }
      }
    })
    .state('app.packages', {
      url: '/packages',
      views: {
        'menuContent': {
          templateUrl: 'templates/packages.html',
          controller: 'ProductsCtrl'
        }
      }
    })	
    .state('app.flowers', {
      url: '/flowers',
      views: {
        'menuContent': {
          templateUrl: 'templates/flowers.html',
          controller: 'ProductsCtrl'
        }
      }
    })		
    .state('app.details', {
      url: '/details/:Type/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/details.html',
          controller: 'DetailsCtrl'
        }
      }
    })	

	
    .state('app.basket', {
      url: '/basket',
      views: {
        'menuContent': {
          templateUrl: 'templates/basket.html',
          controller: 'BasketCtrl'
        }
      }
    })	

	
    .state('app.fastfood', {
      url: '/fastfood',
      views: {
        'menuContent': {
          templateUrl: 'templates/fastfood.html',
          controller: 'FastFoodCtrl'
        }
      }
    })	

	
    .state('app.shops', {
      url: '/shops',
      views: {
        'menuContent': {
          templateUrl: 'templates/shops.html',
          controller: 'ShopsCtrl'
        }
      }
    })	

    .state('app.storemenu', {
      url: '/storemenu/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/store_menu.html',
          controller: 'StoreMenuCtrl'
        }
      }
    })	

	
    .state('app.extras', {
      url: '/extras/:StoreId/:ProductId',
      views: {
        'menuContent': {
          templateUrl: 'templates/extras.html',
          controller: 'ExtrasCtrl'
        }
      }
    })	
	

	.state('app.orderform', {
	  url: '/orderform',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/orderform.html',
		  controller: 'OrderFormCtrl'
		}
	  }
	})		
	
	
	.state('app.superproducts', {
	  url: '/superproducts',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/superproducts.html',
		  controller: 'OrderFormCtrl'
		}
	  }
	})		
	
	
	

	
	.state('app.address', {
	  url: '/address',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/address.html',
		  controller: 'AddressCtrl'
		}
	  }
	})	

	.state('app.supermarkets', {
	  url: '/supermarkets',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/supermarkets.html',
		  controller: 'SuperMarketsCtrl'
		}
	  }
	})	
	
	.state('app.contact', {
	  url: '/contact',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/contact.html',
		  controller: 'ContactCtrl'
		}
	  }
	})		

	.state('app.about', {
	  url: '/about',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/about.html',
		  controller: 'AboutCtrl'
		}
	  }
	})		

	.state('app.specialrequests', {
	  url: '/specialrequests',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/specialrequests.html',
		  controller: 'SpecialRequestsCtrl'
		}
	  }
	})		
	
	.state('app.packageform', {
	  url: '/packageform',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/packageform.html',
		  controller: 'PackageFormCtrl'
		}
	  }
	})		

	.state('app.orders', {
	  url: '/orders',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/orders.html',
		  controller: 'OrdersCtrl'
		}
	  }
	})		

	.state('app.admin', {
	  url: '/admin',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/admin.html',
		  controller: 'AdminOrdersCtrl'
		}
	  }
	})		
	
	.state('app.delivery', {
	  url: '/delivery',
	  views: {
		'menuContent': {
		  templateUrl: 'templates/delivery.html',
		  controller: 'DeliveryCtrl'
		}
	  }
	})		
	
    .state('app.complete', {
      url: '/complete/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/complete.html',
          controller: 'CompleteOrderCtrl'
        }
      }
    })		

    .state('app.orderdetails', {
      url: '/orderdetails/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/orderdetails.html',
          controller: 'OrderDetailsCtrl'
        }
      }
    })	

    .state('app.map', {
      url: '/map/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/map.html',
          controller: 'MapCtrl'
        }
      }
    })	
	
	
  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/address');
});
